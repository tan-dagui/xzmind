package xuanzi.xzmind.client;

import sbaike.client.h5.client.Log;
import xuanzi.h5.fs.core.FileReader;
import xuanzi.h5.fs.core.FileWriter;
import xuanzi.h5.fs.core.IFile;
import xuanzi.h5.fs.core.IFileSystem;
import xuanzi.h5.fs.impl.BFileBytes;
import xuanzi.xzmind.core.StoreSource;

public class FileStoreSource implements StoreSource{

	IFileSystem fs;
	
	public FileStoreSource(IFileSystem fs) {
		this.fs = fs;
	}

	@Override
	public void read(Object path,final Result res) {
		Log.log("FileStoreSource read");
		IFile file = (IFile) path;
		fs.readFile(file, new FileReader() {
			
			@Override
			public void result(BFileBytes fbb) {
				res.result(fbb.getBytes()); 
			} 
		});
	}

	@Override
	public void save(Object path,final String content,final Result res) { 
		Log.log("FileStoreSource save");
		IFile file = (IFile) path;
		fs.writeFile(file, new FileWriter() {
			@Override
			public void finish(BFileBytes fbb) {
				res.result("ok");
			}
			
			@Override
			public String toString() { 
				return content;
			}
		});
	}

	@Override
	public String getName(Object path) {
		IFile file = (IFile) path;
		return file.getName();
	}

	@Override
	public boolean isAutoSave() { 
		return true;
	}

}
